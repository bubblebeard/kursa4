#include<stdio.h>
#include<stdlib.h>

#define MAX_STR 128

typedef struct data {
    int sleeptime;
    int str_len;
    char string[MAX_STR];
} data;

typedef struct Queue {
    int capacity;
    int size;
    int head;
    int tail;
    data *elements;
} Queue;

Queue* createQueue(int maxElements);
data Dequeue(Queue *Q);
int Enqueue(Queue *Q, data element);
data front(Queue *Q);