#include "TCPEchoServer.h"
#include <pthread.h>

#define QUEUE_SIZE 10
#define BUFLEN 10
#define MAXPENDING 5
#define SENDER_SLEEP 4
#define RECEIVER_SLEEP 4
#define TIMEOUT 1
#define MAX_STR 128

struct ports {
    int min;
    int max;
    int tcp_port;
};

Queue* myQueue;
pthread_mutex_t data_mutex;

void* clSender(void*);
void* mngSender(void*);
void* clReceiver(void*);
void* clReceiverThread(void*);
void* mngReceiver(void*);
void* mngReceiverThread(void*);
int CreateUDPServerSocket(int);
int CreateTCPServerSocket(int);
void ProcessMain(int, int, int, int); /* Main program of process */

int main(int argc, char *argv[]) {
    int min_cl_port, max_cl_port, min_mng_port, max_mng_port;
    pthread_t clSender_t, mngSender_t, clReceiver_t, mngReceiver_t;
    struct ports cl_ports, mng_ports;
    
    if (argc != 7) /* Test for correct number of arguments */ {
        fprintf(stderr, "Usage:  %s <TCP_CL_PORT> <TCP_MNG_PORT> <MIN_CL_PORT> <MAX_CL_PORT> <MIN_MNG_PORT> <MAX_MNG_PORT>\n", argv[0]);
        exit(1);
    }
    int tcp_cl_port = atoi(argv[1]);
    int tcp_mng_port = atoi(argv[2]);
    cl_ports.min = atoi(argv[3]);
    cl_ports.max = atoi(argv[4]);
    cl_ports.tcp_port = tcp_cl_port;
    mng_ports.min = atoi(argv[5]);
    mng_ports.max = atoi(argv[6]);
    mng_ports.tcp_port = tcp_mng_port;

    myQueue = createQueue(QUEUE_SIZE);

    pthread_create(&clSender_t, NULL, clSender, &cl_ports);
    pthread_create(&mngSender_t, NULL, mngSender, &mng_ports);
    pthread_create(&clReceiver_t, NULL, clReceiver, &tcp_cl_port);
    pthread_create(&mngReceiver_t, NULL, mngReceiver, &tcp_mng_port);

    ProcessMain(clSender_t, mngSender_t, clReceiver_t, mngReceiver_t);
    exit(1);
}

void* clSender(void* arg) {
    struct ports* cl_ports = (struct ports*) arg;
    int count = cl_ports->max - cl_ports->min + 1;
    int buflen, UDPSock[count], yes = 1, sinlen = sizeof (struct sockaddr_in);
    char buffer[BUFLEN];
    struct sockaddr_in sock_in;

    for (int i = 0; i < count; i++) {
        UDPSock[i] = CreateUDPServerSocket(cl_ports->min + i);
        if (setsockopt(UDPSock[i], SOL_SOCKET, SO_BROADCAST, &yes, sizeof (int)) < 0)
            DieWithError("CreateUDPSocket: setsockopt() failed\n");
    }

    /* -1 = 255.255.255.255 this is a BROADCAST address,
       a local broadcast address could also be used.
       you can comput the local broadcast using NIC address and its NETMASK 
     */

    sprintf(buffer, "%d", cl_ports->tcp_port);
    buflen = strlen(buffer);
    memset(&sock_in, 0, sizeof (sock_in)); /* Zero out structure */
    sock_in.sin_addr.s_addr = htonl(INADDR_BROADCAST); /* send message to 255.255.255.255 */
    sock_in.sin_family = AF_INET;

    for (;;) {
        //pthread_mutex_lock(&data_mutex);
        if (myQueue->size < myQueue->capacity) {
            //pthread_mutex_unlock(&data_mutex);
            printf("clSender: queue is not full. sending broadcast messages...\n");
            for (int i = 0; i < count; i++) {
                sock_in.sin_port = htons(cl_ports->min + i); /* port number */
                if (sendto(UDPSock[i], buffer, buflen, 0, (struct sockaddr *) &sock_in, sinlen) < 0)
                    printf("clSender: sendto() failed\n");
            }
            sleep(SENDER_SLEEP);
        } else {
            //pthread_mutex_unlock(&data_mutex);
            printf("clSender: queue is full. timeout for %d sec\n", SENDER_SLEEP);
            sleep(SENDER_SLEEP);
        }
    }
}

void* mngSender(void* arg) {
    struct ports* cl_ports = (struct ports*) arg;
    int count = cl_ports->max - cl_ports->min + 1;
    int buflen, UDPSock[count];
    int yes = 1;
    char buffer[BUFLEN];
    struct sockaddr_in sock_in;
    int sinlen = sizeof (struct sockaddr_in);

    for (int i = 0; i < count; i++) {
        UDPSock[i] = CreateUDPServerSocket(cl_ports->min + i);
        if (setsockopt(UDPSock[i], SOL_SOCKET, SO_BROADCAST, &yes, sizeof (int)) < 0)
            DieWithError("CreateUDPSocket: setsockopt() failed\n");
    }

    /* -1 = 255.255.255.255 this is a BROADCAST address,
       a local broadcast address could also be used.
       you can comput the local broadcast using NIC address and its NETMASK 
     */

    sprintf(buffer, "%d", cl_ports->tcp_port);
    buflen = strlen(buffer);
    memset(&sock_in, 0, sizeof (sock_in)); /* Zero out structure */
    sock_in.sin_addr.s_addr = htonl(INADDR_BROADCAST); /* send message to 255.255.255.255 */
    sock_in.sin_family = AF_INET;

    for (;;) {
        //pthread_mutex_lock(&data_mutex);
        if (myQueue->size > 0) {
            //pthread_mutex_unlock(&data_mutex);
            printf("mngSender: queue is not empty. sending broadcast messages...\n");
            for (int i = 0; i < count; i++) {
                sock_in.sin_port = htons(cl_ports->min + i); /* port number */
                if (sendto(UDPSock[i], buffer, buflen, 0, (struct sockaddr *) &sock_in, sinlen) < 0)
                    printf("mngSender: sendto() failed\n");
            }
            sleep(SENDER_SLEEP);
        } else {
            //pthread_mutex_unlock(&data_mutex);
            printf("mngSender: queue is empty. timeout for %d sec\n", SENDER_SLEEP);
            sleep(SENDER_SLEEP);
        }
    }
}

void* clReceiver(void* arg) {
    int *Port = (int*) arg;
    int clntSock; /* Socket descriptor for client connection */
    pthread_t ThreadID;
    int TCPSock = CreateTCPServerSocket(*Port);
    for (;;) /* Run forever */ {
        //pthread_mutex_lock(&data_mutex);
        if (myQueue->size < myQueue->capacity) {
                //pthread_mutex_unlock(&data_mutex);
                clntSock = AcceptTCPConnection(TCPSock);
                if (pthread_create(&ThreadID, NULL, clReceiverThread, &clntSock) != 0)
                    DieWithError("pthread_create() failed\n");
                printf("clReceiver: creating thread...\n");
                sleep(TIMEOUT);
        } else {
            //pthread_mutex_unlock(&data_mutex);
            printf("clReceiver: Queue is full. timeout for %d sec\n", TIMEOUT);
            sleep(TIMEOUT);
        }
    }
}

void* clReceiverThread(void* arg) {
    int clntSock = * (int *) arg; /* Socket descriptor for client connection */
    int recvMsgSize, rcv_time, rcv_len;
    char* rcv_string;
    data elem;

    /* Guarantees that thread resources are deallocated upon return */
    pthread_detach(pthread_self());

    pthread_mutex_lock(&data_mutex);
    if (myQueue->size < myQueue->capacity) {
        printf("clReceiverThread%d: Queue size - %d\n", clntSock, myQueue->size);
        fflush(stdout);
        printf("clReceiverThread%d: queue is not full. Receiving data...\n", clntSock);
        if ((recvMsgSize = recv(clntSock, &rcv_time, sizeof (int), 0)) < 0)
            DieWithError("recv(sleeptime) failed\n");
        printf("clReceiverThread%d: Data(sleeptime) - %d\n", clntSock, rcv_time);
        if ((recvMsgSize = recv(clntSock, &rcv_len, sizeof (int), 0)) < 0)
            DieWithError("recv(str_len) failed\n");
        printf("clReceiverThread%d: Data(str_len) - %d\n", clntSock, rcv_len);

        rcv_string = (char *) malloc(sizeof (char) * rcv_len);
        if (recvMsgSize = recv(clntSock, rcv_string, sizeof (char) * rcv_len, 0) < 0)
            DieWithError("recv(string) failed\n");
        printf("clReceiverThread%d: Data(string) - %s\n", clntSock, rcv_string);
        elem.sleeptime = rcv_time;
        elem.str_len = rcv_len;
        strcpy(elem.string, rcv_string);
        free(rcv_string);
        Enqueue(myQueue, elem);
        pthread_mutex_unlock(&data_mutex);
        close(clntSock);
    } else {
        close(clntSock);
        pthread_mutex_unlock(&data_mutex);
        printf("clReceiverThread%d: queue is full. closing connection...\n", clntSock);
    }
    return (NULL);
}

void* mngReceiver(void* arg) {
    int *Port = (int*) arg;
    int clntSock; /* Socket descriptor for client connection */
    pthread_t ThreadID;
    int TCPSock = CreateTCPServerSocket(*Port);
    for (;;) /* Run forever */ {
        //pthread_mutex_lock(&data_mutex);
        if (myQueue->size > 0) {
            //pthread_mutex_unlock(&data_mutex);
            clntSock = AcceptTCPConnection(TCPSock);
            if (pthread_create(&ThreadID, NULL, mngReceiverThread, &clntSock) != 0)
                DieWithError("pthread_create() failed\n");
            printf("mngReceiver: creating thread...\n");
            sleep(TIMEOUT);
        } else {
            //pthread_mutex_unlock(&data_mutex);
            printf("mngReceiver: timeout for %d sec\n", TIMEOUT);
            sleep(TIMEOUT);
        }
    }
}

void* mngReceiverThread(void* arg) {
    int clntSock = * (int *) arg; /* Socket descriptor for client connection */
    int sndMsgSize;
    data elem;
    /* Guarantees that thread resources are deallocated upon return */
    pthread_detach(pthread_self());

    pthread_mutex_lock(&data_mutex);
    if (myQueue->size > 0) {
        printf("mngReceiverThread%d: Queue size - %d\n", clntSock, myQueue->size);
        fflush(stdout);
        elem = Dequeue(myQueue);
        printf("mngReceiverThread%d: queue is not empty. sending data...\n", clntSock);
        if ((send(clntSock, &elem.sleeptime, sizeof (int), 0)) != sizeof (int))
            DieWithError("send(sleeptime) sent a different number of bytes than expected\n");
        printf("mngReceiverThread%d: Data(sleeptime) - %d\n", clntSock, elem.sleeptime);
        if ((send(clntSock, &elem.str_len, sizeof (int), 0)) != sizeof (int))
            DieWithError("send(str_len) sent a different number of bytes than expected\n");
        printf("mngReceiverThread%d: Data(str_len) - %d\n", clntSock, elem.str_len);       
        if ((send(clntSock, elem.string, sizeof (char) * elem.str_len, 0)) != sizeof (char) * elem.str_len)
            DieWithError("send(string) sent a different number of bytes than expected\n");
        printf("mngReceiverThread%d: Data(string) - %s\n", clntSock, elem.string);
        fflush(stdout);
        pthread_mutex_unlock(&data_mutex);
        close(clntSock);
    } else {
        close(clntSock);
        pthread_mutex_unlock(&data_mutex);
        printf("mngReceiverThread%d: queue is empty. closing connection\n", clntSock);
    }
    return (NULL);
}

void ProcessMain(int clSender_t, int mngSender_t, int clReceiver_t, int mngReceiver_t) {
    pthread_exit(&clSender_t);
    pthread_exit(&mngSender_t);
    pthread_exit(&clReceiver_t);
    pthread_exit(&mngReceiver_t);
    return;
}

int CreateUDPServerSocket(int port) {
    int sock; /* socket to create */
    struct sockaddr_in echoServAddr; /* Local address */

    /* Create socket for incoming connections */
    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("CreateUDPSocket: socket() failed\n");

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof (echoServAddr)); /* Zero out structure */
    echoServAddr.sin_family = AF_INET; /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST); /* Any incoming interface */
    echoServAddr.sin_port = htons(port); /* Local port */
    return sock;
}

int CreateTCPServerSocket(int port) {
    int sock; /* socket to create */
    struct sockaddr_in echoServAddr; /* Local address */

    /* Create socket for incoming connections */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("CreateTCPSocket: socket() failed\n");

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof (echoServAddr)); /* Zero out structure */
    echoServAddr.sin_family = AF_INET; /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(port); /* Local port */

    /* Bind to the local address */
    if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof (echoServAddr)) < 0)
        DieWithError("CreateTCPSocket: bind() failed\n");

    /* Mark the socket so it will listen for incoming connections */
    if (listen(sock, MAXPENDING) < 0)
        DieWithError("CreateTCPSocket: listen() failed\n");

    return sock;
}