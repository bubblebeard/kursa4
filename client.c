#include <sys/types.h>
#include <netinet/in.h>
#include <ctype.h>
#include "TCPEchoServer.h"

#define SLEEPTIME 5
#define MIN_PORT 9081
#define MAX_PORT 9090
#define MAX_STR 128
#define DGRAM_LEN 512

void DieWithError(char *errorMessage); /* Error handling function */
int CreateTCPConnection(struct sockaddr_in echoServAddr, int ServPort); /* Creating TCP Connection */

int main(int argc, char **argv) {
    struct sockaddr_in si_me, si_other; /* Structure for address */
    int port, recv_len, addrlen = sizeof (si_other);
    int len_inet; /* length */
    int sock; /* Socket */
    int sleeptime, bind_t = -1;
    unsigned short min_port, max_port; /* Broadcast Port */
    char dgram[DGRAM_LEN]; /* Recv buffer */

    /* Test for correct number of arguments */
    if (argc > 3 || argc == 2) {
        fprintf(stderr, "Usage: %s [Min Port, Max Port]\n", argv[0]);
        exit(1);
    }
    if (argc == 3) {
        min_port = atoi(argv[1]);
        max_port = atoi(argv[2]);
    } else {
        min_port = MIN_PORT;
        max_port = MAX_PORT;
    }
    srand(getpid());
    int port_count = max_port - min_port + 1;
    /* Create a UDP socket to use: */
    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() for UDP failed\n");

    /* Zero out the structure */
    memset((char *) &si_me, 0, sizeof (si_me));

    si_me.sin_family = AF_INET;
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    for (int i = 0; i < port_count && bind_t != 0; i++) {
        si_me.sin_port = htons(min_port + i);
        //bind socket to port
        if (bind(sock, (struct sockaddr*) &si_me, sizeof (si_me)) < 0) {
            printf("Client %d: unable to bind socket to port %d\n", getpid(), min_port + i);
        } else {
            bind_t = 0;
            printf("Client %d: binded to port %d\n", getpid(), min_port + i);
        }
    }
    if (bind_t == -1) {
        printf("Client %d: unable to bind socket to any of selected ports\n", getpid());
        return (EXIT_FAILURE);
    }
    for (;;) {
        /*
         * Wait for a broadcast message:
         */
        recv_len = recvfrom(sock, dgram, sizeof dgram, 0, (struct sockaddr *) &si_other, &addrlen);
        port = atoi(dgram);

        if (recv_len < 0)
            DieWithError("recvfrom(2) failed\n"); /* else err */
        //print details of the server and the data received
        printf("Client %d: receiving packet from %s:%ho\nData: %d\n", getpid(), inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port), port);
        fflush(stdout);
        if (port <= 0) {
            printf("Client %d: received data is invalid\n", getpid());
            fflush(stdout);
        } else {
            sleeptime = CreateTCPConnection(si_other, port);
            sleep(sleeptime);
        }
    }
    return 0;
}

int CreateTCPConnection(struct sockaddr_in ServAddr, int ServPort) {
    int clntsock; /* Socket to create */
    struct sockaddr_in echoServAddr; /* Server address */
    int sleeptime, str_len;
    char* string;

    char SET[] = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm0123456789";
    int set, setlen = strlen(SET);

    /* Create socket for connection with server */
    if ((clntsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("socket() for TCP failed\n");

    /* Construct server address structure */
    memset(&echoServAddr, 0, sizeof (echoServAddr)); /* Zero out structure */
    echoServAddr.sin_family = AF_INET; /* Internet address family */
    echoServAddr.sin_addr = ServAddr.sin_addr;
    echoServAddr.sin_port = htons(ServPort); /* Local port */

    unsigned int servLen; /* Length of server address data structure */

    /* Set the size of the in-out parameter */
    printf("Client %d: creating connection\n", getpid());
    fflush(stdout);

    /* Connecting to server */
    if ((connect(clntsock, (struct sockaddr *) &echoServAddr, sizeof (echoServAddr))) < 0)
        DieWithError("connect() failed\n");
    printf("Client %d: connected to a server %s:%ho\n", getpid(), inet_ntoa(echoServAddr.sin_addr), ntohs(echoServAddr.sin_port));
    fflush(stdout);

    /* Generating random string, sleep time and sending it to server */
    sleeptime = rand() % SLEEPTIME;
    str_len = rand() % (MAX_STR - 2) + 2;
    string = malloc(sizeof (char)*str_len);
    for (int i = 0; i < str_len; i++) {
        set = rand() % setlen;
        string[i] = SET[set];
    }
    string[str_len - 1] = '\0';

    /* Sent string and sleep time */
    if (send(clntsock, &sleeptime, sizeof (int), 0) != sizeof (int))
        DieWithError("send(sleeptime) sent a different number of bytes than expected\n");
    if (send(clntsock, &str_len, sizeof (int), 0) != sizeof (int))
        DieWithError("send(strlen) sent a different number of bytes than expected\n");
    if (send(clntsock, string, sizeof (char) * str_len, 0) != sizeof (char) * str_len)
        DieWithError("send(string) sent a different number of bytes than expected\n");
    printf("Client %d: Sending data to server.\nsleeptime - %d\nstr_len - %d\nstring - %s\n", getpid(), sleeptime, str_len, string);
    fflush(stdout);
    free(string);
    close(clntsock); /* Close connection */
    return sleeptime;
}