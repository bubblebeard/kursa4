#include "queue.h"

Queue * createQueue(int maxElements) {
    /* Create a Queue */
    Queue *Q;
    Q = (Queue *) malloc(sizeof (Queue));
    /* Initialise its properties */
    Q->elements = (data *) malloc(sizeof (data) * maxElements);
    Q->size = 0;
    Q->capacity = maxElements;
    Q->head = 0;
    Q->tail = -1;
    /* Return the pointer */
    return Q;
}

data Dequeue(Queue *Q) {
    data elem;
    /* If Queue size is zero then it is empty. So we cannot pop */
    if (Q->size == 0) {
        printf("Queue is Empty\n");
        return;
    }/* Removing an element is equivalent to incrementing index of front by one */
    else {
        elem = Q->elements[Q->head];
        Q->size--;
        Q->head++;
        /* As we fill elements in circular fashion */
        if (Q->head == Q->capacity) {
            Q->head = 0;
        }
    }
    return elem;
}

data front(Queue *Q) {
    if (Q->size == 0) {
        printf("Queue is Empty\n");
        return;
    }
    /* Return the element which is at the front*/
    return Q->elements[Q->head];
}

int Enqueue(Queue *Q, data element) {
    /* If the Queue is full, we cannot push an element into it as there is no space for it.*/
    if (Q->size == Q->capacity) {
        printf("Queue is Full\n");
        return -1;
    } else {
        Q->size++;
        Q->tail++;
        /* As we fill the queue in circular fashion */
        if (Q->tail == Q->capacity) {
            Q->tail = 0;
        }
        /* Insert the element in its rear side */
        Q->elements[Q->tail] = element;
    }
    return 0;
}