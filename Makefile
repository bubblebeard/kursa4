all:	client.c DieWithError.c manager.c server.c AcceptTCPConnection.c queue.c
	gcc -std=c99 client.c DieWithError.c -o client
	gcc -std=c99 manager.c DieWithError.c -o manager
	gcc -pthread -std=c99 server.c AcceptTCPConnection.c DieWithError.c queue.c -o server
clean:
	rm -f client manager server
runserver:
	./server 9110 9120 9081 9090 9091 9100
runcl:
	./client
runmng:
	./manager
